import Vue from 'vue';

/* 引入 ant-design-vue 组件 */
import {
    ConfigProvider,Layout,Modal,Tabs,Form,Input,Checkbox,Icon,Divider,
    Button,Menu,Avatar,Card,Spin
} from 'ant-design-vue';

Vue.prototype.$modal = Modal;

import $message from 'ant-design-vue/lib/message/index';
import 'ant-design-vue/lib/message/style/index';
Vue.prototype.$message = $message;

Vue.use(ConfigProvider);
Vue.use(Layout);
Vue.use(Modal);
Vue.use(Tabs);
Vue.use(Form);
Vue.use(Input);
Vue.use(Checkbox);
Vue.use(Icon);
Vue.use(Divider);
Vue.use(Button);
Vue.use(Menu);
Vue.use(Avatar);
Vue.use(Card);
Vue.use(Spin);

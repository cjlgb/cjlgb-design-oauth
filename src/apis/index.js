import axIos from 'axios';
import $md5 from 'js-md5';

/**
 * 账号登陆
 * @param params
 * @returns {Promise<AxiosResponse<T>>}
 */
export function login(params) {
    //  将密码进行加密,并添加随机盐参数
    let salt = new Date().getTime();
    params['password'] = $md5($md5(params['password']) + salt);
    params['salt'] = salt;
    return axIos.post("/apis/cjlgb-design-oauth/grant/login", params);
}

/**
 * 获取当前用户的信息
 * @returns {Promise<AxiosResponse<T>>}
 */
export function getMyInfo() {
    return axIos.get("/apis/cjlgb-design-oauth/user/getMyInfo");
}

/**
 * 账号注册
 * @param params
 * @returns {Promise<AxiosResponse<T>>}
 */
export function register(params) {
    //  将密码进行加密
    params['password'] = $md5(params['password']);
    return axIos.post("/apis/cjlgb-design-oauth/user/register", params);
}

/**
 * 根据Id查询Oauth客户端
 * @param clientId
 * @returns {Promise<AxiosResponse<T>>}
 */
export function getOauthClient(clientId) {
    return axIos.get("/apis/cjlgb-design-developer/client/" + clientId);
}

/**
 * 申请授权码
 * @param clientId
 * @returns {Promise<AxiosResponse<T>>}
 */
export function getGrantCode(clientId) {
    return axIos.post("/apis/cjlgb-design-oauth/grant/apply/code",{ client_id  : clientId });
}

import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from './store'

/* 关闭警告 */
Vue.config.productionTip = false;
/* Ant Design 组件引用配置 */
import './core/antd';

/* axIos 配置 */
import './core/axios-conf';


new Vue({
    router,
    store,
    render: h => h(App)
}).$mount("#app");

const webpack = require('webpack');

module.exports = {
    publicPath: process.env.APP_PUBLIC_PATH,
    css: {
        loaderOptions: {
            less: {
                javascriptEnabled: true
            }
        }
    },
    configureWebpack: {
        plugins: [
            //  忽略moment.js的所有区域设置文件
            new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
        ],
    },
    //  跨域配置
    devServer: {
        port: 8001,
        host: 'cjlgb-design-oauth',
        //  关闭Host检查
        disableHostCheck: true,
        proxy: {
            '/apis': {
                target: 'http://cjlgb-design-gateway:10001',
                ws: true,
                changeOrigin: true,
                pathRewrite: {
                    '^/apis': ''
                }
            }
        }
    }
};

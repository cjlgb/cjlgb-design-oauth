<p align="center">
 <img src="https://img.shields.io/badge/Cjlgb%20Design%20Security-1.0.0-brightgreen" alt="Build Status">
 <img src="https://img.shields.io/badge/Ant%20Design%20Vue-1.5.6-brightgreen" alt="Build Status">
 <img src="https://img.shields.io/badge/Spring%20Cloud-Hoxto.SR5-blue.svg" alt="Coverage Status">
 <img src="https://img.shields.io/badge/Spring%20Boot-2.3.1.RELEASE-blue.svg" alt="Downloads">
</p>

- 基于 Spring Cloud Hoxton 、Spring Boot 2.2、 OAuth2 的RBAC权限管理系统  
- 基于数据驱动视图的理念封装 Ant Design Vue，即使没有 vue 的使用经验也能快速上手
- 提供 lambda 、stream api 、webflux 的生产实践   


#### <a target="_blank" href="https://cjlgb-design-oauth.cdn.bcebos.com/video%2F20200703003537.mp4">预览</a>
- 开放平台：https://gitee.com/cjlgb/cjlgb-design-developer
- 单点登录：https://gitee.com/cjlgb/cjlgb-design-oauth
- 后端代码：https://gitee.com/cjlgb/cjlgb-cloud-platform


#### 核心依赖

依赖 | 版本
---|---
Spring Boot |  2.3.1.RELEASE
Spring Cloud | Hoxton.SR5
Mybatis Plus | 3.3.2
Ant Design Vue | 1.5.6
   

#### 安装依赖
```
yarn install
```

#### 启动服务
```
yarn serve
```

#### 打包
```
yarn build
```

#### 开源共建

1. 欢迎提交 [pull request](https://gitee.com/cjlgb/cjlgb-cloud-platform/pulls)，注意对应提交对应 `dev` 分支

2. 欢迎提交 [issue](https://gitee.com/cjlgb/cjlgb-cloud-platform/issues)，请写清楚遇到问题的原因、开发环境、复显步骤。

3. 不接受`功能请求`的 [issue](https://gitee.com/cjlgb/cjlgb-cloud-platform/issues)，功能请求可能会被直接关闭。  

4. QQ: <a target="_blank" href="http://wpa.qq.com/msgrd?v=3&uin=2055305009&site=qq&menu=yes">2055305009</a>    





